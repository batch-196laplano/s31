//Client
//creates requests from a server

//server
//host and deliver resources requested by a client

//node.js


// console.log("Hello World!");
//require is a builtin js method which imports packages
//http is the default package, it allows us to use methods that let us create servers
let http = require("http");

// http is a module(packages we imported)

//http on URL protocol to client server
//http://localhost:4000
//console.log(http);

//createServer is a method from http module
//.createServer(function(request,response){})
http.createServer(function(request,response){

	// console.log(request.url)//contains the endpoint

	if(request.url=== "/"){
		// /-default endpoint
		//response.writeHead()-is a method allows headers to our response
		//2 arguments 1.StatusCode(200 means ok or 404 means results cannot be found,etc),(content type)
		response.writeHead(200,{'Content-Type':'text/plain'});
		//.end() - ends our response, sends data/message as a string
		response.end("Hello from our first Server! This is from / endpoint.");
	} else if (request.url=== "/profile"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Hi I'm Denver");
	}


//.listen() allows us o assign a port to our server 4000,4040,8000,5000,3000,4200
}).listen(4000);


console.log("Server is running on localHost:4000!")

// ctrl+c turnoff server


